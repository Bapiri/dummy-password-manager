import psycopg2

from random_digit import digit_generator


class PasswordManager:
    def __init__(self):
        self.host = "localhost"
        self.port = "5432"
        self.password = "123456"
        self.db_name = "pwds"
        self.user = "postgres"
        try:
            self.connection = psycopg2.connect(
                host=self.host,
                database=self.db_name,
                user=self.user,
                password=self.password,
                port=self.port
            )
            self.connection.autocommit = True
            self.cursor = self.connection.cursor()
        except:
            print("Connection failed.")

        print("""
            1- Insert new record
            2- Display all rows
            3- Display a specific row
        """)
        choice = int(input("Enter the number: "))
        if choice == 1:
            self.insert_new_record()
        elif choice == 2:
            self.select_all_rows()
        elif choice == 3:
            self.select_specific_row()

    def create_table(self):
        create_table_query = """
        CREATE TABLE pwds(
            id serial PRIMARY KEY,
            NAME TEXT NOT NULL,
            WEBSITE TEXT NOT NULL,
            PASSWORD TEXT NOT NULL,
            DESCRIPTION TEXT
        )
        """
        self.cursor.execute(create_table_query)
        self.connection.commit()

    def insert_new_record(self):
        random_digit = digit_generator()
        record = list(map(str, input(
            "split with space: << name website password description >>: \n").split(" ")))
        insert_query = "INSERT INTO pwds(id, name, website, password, description) VALUES ('" + random_digit + \
            "', '" + record[0] + "', '" + record[1] + \
            "', '" + record[2] + "', '" + record[3] + "')"
        self.cursor.execute(insert_query)
        self.connection.commit()

    def select_all_rows(self):
        select_query = "SELECT * from pwds"
        self.cursor.execute(select_query)
        rows = self.cursor.fetchall()
        for r in rows:
            print("Record: {0}".format(r))

    def select_specific_row(self):
        website = input("Type website: ")
        select_query = "SELECT * from pwds WHERE website='{website}'".format(
            website=website)
        self.cursor.execute(select_query)
        rows = self.cursor.fetchall()
        for r in rows:
            print("Record: {0}".format(r))


password_manager = PasswordManager()
