import random
import string


def digit_generator(size=5):
    random_choice = (random.choice(string.digits) for _ in range(size))
    return ''.join(random_choice)